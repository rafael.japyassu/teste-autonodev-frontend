export interface INeighborhoodObject {
  id: number;
  name: string;
}

export default interface INeighborhood {
  zone: number;
  neighborhood: INeighborhoodObject[];
}