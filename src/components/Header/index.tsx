import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { FaSearch, FaSignInAlt } from 'react-icons/fa'

import './styles.scss'

const Header: React.FC = () => {
  return (
    <Navbar bg="dark" variant="dark" expand="lg" className="app-header">
      <Navbar.Brand href="#home">AutonoDev Test</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="#home" className="link-icon">
            <FaSearch /> Pesquisar Bairro
          </Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link href="#home" className="link-icon">
            <FaSignInAlt /> Entrar
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default Header;