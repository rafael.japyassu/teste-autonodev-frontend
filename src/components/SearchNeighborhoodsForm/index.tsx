import React, { ChangeEvent } from 'react';
import { Button, Form, FormControl, InputGroup } from 'react-bootstrap';
import { FaSearch } from 'react-icons/fa';

import './styles.scss'

interface IProps {
  onSearch(e: ChangeEvent<HTMLFormElement>): void;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
  neighborhood: string;
}

const SearchNeighborhoodsForm: React.FC<IProps> = (props) => {
  const { onSearch, onChange, neighborhood } = props

  return (
    <Form onSubmit={onSearch} className="form-search-neighborhoods">
      <h3>Pesquise um Bairro</h3>
      <InputGroup className="mb-3">
        <FormControl
          placeholder="ex.: Aeroclube..."
          value={neighborhood}
          required
          onChange={onChange}
        />
        <InputGroup.Append>
          <Button variant="dark" type="submit">
            <FaSearch /> Pesquisar
          </Button>
        </InputGroup.Append>
      </InputGroup>
    </Form>
  );
}

export default SearchNeighborhoodsForm;