import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import ListNeighborhoods from '../../containers/ListNeighborhoods';

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={ListNeighborhoods} />
      <Redirect from="*" to="/" />
    </Switch>
  );
}
