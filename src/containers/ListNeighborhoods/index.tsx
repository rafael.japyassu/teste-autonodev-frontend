import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import INeighborhood, { INeighborhoodObject } from '../../interfaces/neighborhood';
import { neighborhoods } from '../../data/neighborhoods.json';
import { Container } from 'react-bootstrap';
import SearchNeighborhoodsForm from '../../components/SearchNeighborhoodsForm';

interface INeighborhoodSelect {
  zone: number;
  neighborhood: INeighborhoodObject;
}

const ListNeighborhoods: React.FC = () => {
  const [neighborhoodsList, ] = useState<INeighborhood[]>(neighborhoods);
  const [neighborhoodsSelect, setNeighborhoodsSelect] = useState<INeighborhoodSelect>();
  const [neighborhood, setNeighborhood] = useState('')

  const onSearch = useCallback((e: ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();

    const result = neighborhoods.filter(neighborhoodList => {
      const result = neighborhoodList.neighborhood.filter(item => (
        item.name !== neighborhood
      ))
      return neighborhoodList.neighborhood.indexOf(result[0])
    })

    const neighborhoodResult =  result[0].neighborhood.filter(item => (
      item.name === neighborhood
    ))
    setNeighborhoodsSelect({ zone: result[0].zone, neighborhood: neighborhoodResult[0] })

  }, [neighborhood])

  useEffect(() => {
    console.log(neighborhoodsList)
  }, [neighborhoodsList])

  return (
    <div className="page-list-neighborhoods">
      <Container>
        <SearchNeighborhoodsForm
          neighborhood={neighborhood}
          onChange={(e: ChangeEvent<HTMLInputElement>) => setNeighborhood(e.target.value)}
          onSearch={onSearch}
        />
      </Container>
    </div>
  );
}

export default ListNeighborhoods;