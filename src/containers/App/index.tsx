import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import Header from '../../components/Header';

import Routes from '../../routes/__Routes';

const App = () => (
  <BrowserRouter>
    <div className="App">
      <Header />
      <Switch>
        <Routes />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
